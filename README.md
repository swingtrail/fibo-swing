# Fibo Swing

![](https://i.imgur.com/Nr4WHxg.png)

Fibo Swing is an auto fibonacci extension to help you easier to analyst what trend happening now based on swing period.  
This fibonacci is designed to simplify swing trading, so you can't setup your own configuration value.

## HOW TO USE
1. Add [Fibo Swing](https://www.tradingview.com/script/D22dWvuW-aalfiann-Fibo-Swing/) to Favorite.
2. Entry Long or Short in the red or orange zone then setup taking profit at green zone.

---

Author: M ABD AZIZ ALFIAN  
Twitter: [@aalfiann](https://twitter.com/aalfiann)
